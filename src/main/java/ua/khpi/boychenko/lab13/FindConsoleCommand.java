package ua.khpi.boychenko.lab13;

import ua.khpi.boychenko.lab11.Item;
import ua.khpi.boychenko.lab11.View;
import ua.khpi.boychenko.lab11.ViewResult;
import java.util.*;
import java.util.InputMismatchException;
import java.util.Scanner;


/**
 * Консольна команда Find; шаблон Command
 * 
 * @author Бойченко Олександра
 * @version 1.0
 */
public class FindConsoleCommand implements ConsoleCommand {

	/**
	 * Об'єкт, який реалізує інтерфейс {@linkplain View}; 
	 * Обслуговує колекцію об'єктів
	 */
	private View view;




	/**
	 * Повертає поле {@linkplain FindConsoleCommand # view}
	 * 
	 * @Return значення {@linkplain FindConsoleCommand # view}  
	 */
	public View getView() {
		return view;
	}

	/**
	 * Встановлює поле {@linkplain FindConsoleCommand # view}
	 * 
	 * @Param view значення для {@linkplain FindConsoleCommand # view}  
	 * @Return нове значення {@linkplain FindConsoleCommand # view}  
	 */
	public View setView(View view) {
		return this.view = view;
	}

	/**
	 * Ініціалізує поле {@linkplain FindConsoleCommand # view}
	 * 
	 * @Param view об'єкт, який реалізує інтерфейс {@linkplain View}  
	 */
	public FindConsoleCommand(View view) {
		this.view = view;
	}

	@Override
	public char getKey() {
		return 'f';
	}

	@Override
	public String toString() {
		return "'f'ind";
	}

	@Override
	public void execute() {
		int index = 0;
		Scanner in = new Scanner(System.in);
		System.out.println("Input index of element.");
		try {
			index = in.nextInt();
			if (index > ((ViewResult) view).getItems().size())
				throw (new IndexOutOfBoundsException());

		} catch (IndexOutOfBoundsException e) {
			System.out.println("Error input value: Index is out of range!");
			System.exit(0);
		}
		search(index);

	}
	
	/**
	 * Шукає елемент за індексом
	 * 
	 * @Param index індекс елементу, який шукаємо  
	 * @return item знайдений елемент
	 */
	public Item search(int index) {
		int i = 0;
		Item item = new Item();
		for (Item temp : ((ViewResult) view).getItems()) {
			if (index == i) {
				System.out.printf("Found element: x = %f | y = %d\n", temp.getX(), temp.getY());
				item = temp;
			}
			i++;
		}
		return item;
	}
}
