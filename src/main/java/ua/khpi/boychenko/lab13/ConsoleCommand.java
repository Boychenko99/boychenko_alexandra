package ua.khpi.boychenko.lab13;
/**
 * Інтерфейс Консольної команди; Шаблон Command
 * 
 * @Author Бойченко Олександра
 * @Version 1.0  
 */
public interface ConsoleCommand extends Command {

	/**
	 * Гаряча клавіша команди; Шаблон Command
	 * 
	 * @Return символ гарячої клавіші  
	 */
	public char getKey();
}
