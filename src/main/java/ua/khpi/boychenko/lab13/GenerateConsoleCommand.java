package ua.khpi.boychenko.lab13;

import ua.khpi.boychenko.lab11.View;

import java.util.InputMismatchException;
import java.util.Scanner;



/**
 * Консольна команда Generate; шаблон Command
 * 
 * @author Бойченко Олександра
 * @version 1.0
 */
public class GenerateConsoleCommand implements ConsoleCommand {


	private View view;

	/**
	 * Ініціалізує поле {@linkplain GenerateConsoleCommand # view}
	 * 
	 * @Param view об'єкт, який реалізує інтерфейс {@linkplain View}  
	 */
	public GenerateConsoleCommand(View view) {
		this.view = view;
	}

	@Override
	public char getKey() {
		return 'g';
	}

	@Override
	public String toString() {
		return "'g'enerate";
	}

	@Override
	public void execute() {
		int x = 0;
		Scanner in = new Scanner(System.in);
		System.out.println("Calculation.");
		System.out.println("Input 'x' number for calculation.");
		try {
			x = in.nextInt();
		} catch (InputMismatchException e) {
			System.out.println("Error input value: " + e);
			System.exit(0);
		}

		view.viewInit(x);
		view.viewShow();

	}

}
