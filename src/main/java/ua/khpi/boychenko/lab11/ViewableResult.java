package ua.khpi.boychenko.lab11;

/**
 * ConcreteCreator (Шаблон проектування Factory Method) Оголошує метод
 *  "Фабрика", який створює об'єкти
 *
 * @Author Бойченко Олександра 
 * @Version 1.0
 * @See Viewable
 * @See ViewableResult#getView ()  
 */
public class ViewableResult implements Viewable {
    /** Створює відображаємий об'єкт {@linkplain ViewResult} */
    @Override
    public View getView() {

        return new ViewResult();
    }

}