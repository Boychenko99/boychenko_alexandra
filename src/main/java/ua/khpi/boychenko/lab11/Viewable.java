package ua.khpi.boychenko.lab11;

/**
 * Creator (Шаблон проектуванняFactory Method) оголошує метод, "Фабрика" який
 * створює об'єкти
 *
 * @Author Бойченко Олександра
 * @Version 1.0
 * @See Viewable#getView()  
 */
public interface Viewable {

    /** Створює об'єкт, який реалізує {@linkplain View} */
    public View getView();
}
