package ua.khpi.boychenko.lab11;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Обчислення і видображення результатів. Містить реалізацію статичного методу
 * main().
 *
 * @author Бойченко Олександра
 * @version 2.0
 * @see Main#main
 */
public class Main {

    /** Об'єкт, який реалізує інтерфейс {@linkplain View};
      * Обслуговує колекцію об'єктів
      */
    private View view;

    public Main(View view) {
        this.view = view;
    }

    /** Зображення меню. */
    protected void menu() {
        String s = null;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        Scanner inv = new Scanner(System.in);
        int x = 0;
        do {
            do {
                System.out.println("Enter command...");
                System.out.print(
                        "'q' for quit program, 'v' for show resul, 'g' for find result, 's' for saving, 'r' for restoring: ");
                try {
                    s = in.readLine();
                } catch (IOException e) {
                    System.out.println("Error: " + e);
                    System.exit(0);
                }
            } while (s.length() != 1);
            switch (s.charAt(0)) {
                case 'q':
                    System.out.println("Exit.");
                    break;
                case 'v':
                    System.out.println("View current.");
                    view.viewShow();
                    break;
                case 'g':
                    System.out.println("Calculation.");
                    System.out.println("Input 'x' number for calculation.");
                    try {
                        x = inv.nextInt();
                    } catch (InputMismatchException e) {
                        System.out.println("Error input value: " + e);
                        System.exit(0);
                    }

                    view.viewInit(x);
                    view.viewShow();
                    break;
                case 's':
                    System.out.println("Save current.");
                    try {
                        view.viewSave();
                    } catch (IOException e) {
                        System.out.println("Serialization error: " + e);
                    }
                    view.viewShow();
                    break;
                case 'r':
                    System.out.println("Restore last saved.");
                    try {
                        view.viewRestore();
                    } catch (Exception e) {
                        System.out.println("Serialization error: " + e);
                    }
                    view.viewShow();
                    break;
                default:
                    System.out.print("Wrong command. ");
            }
        } while (s.charAt(0) != 'q');
    }

    /**
     * Виконується при запуску програми. Визначається значення функції для
     * різних аргументів. Результати розрахунків виводяться на екран.
     *
     * @param args - параметри запуску програми.
     */
    public static void main(String[] args) {

        Main control = new Main(new ViewableResult().getView());
        control.menu();
    }
}

