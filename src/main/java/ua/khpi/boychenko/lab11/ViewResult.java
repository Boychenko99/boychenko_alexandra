package ua.khpi.boychenko.lab11;


import java.io.*;
import java.util.ArrayList;

/**
 * ConcreteProduct (Шаблон проектування Factory Method)
  * Обчислення функції, збереження і відображення результатів
 *
  * @Author Бойченко Олександра
  * @Version 1.0
  * @See View
  */
public class ViewResult implements View {
    /** Ім'я файлу, яке використовується при сериалізации. */
    private static final String FNAME = "Item.bin";
    /** Контейнер результаті обчислення функціі та її аргументів. {@linkplain Item} */
    private ArrayList<Item> container = new ArrayList<Item>();

    /**
     * Конструктор за замовчуванням
     */
    public ViewResult() {

    }

    /**
     * Отримання значення {@linkplain ViewResult # container}
     *
     * @return поточне значення посилання на об'єкт {@linkplain ArrayList}
     */
    public ArrayList<Item> getItems() {
        return container;
    }

    /**
     * Обчислення наибольшої длини послідовності 1 в двоїчному представлення
     * цілісної суми квадрата і куби 10 * cos (α).
     *
     * @param x - аргумент обчисляємой функціі
     * @return res - результат обчислення
     */
    private int calculation(int x) {
        double y = 0;
        y = 10 * Math.pow(Math.cos(x * Math.PI / 180), 2) + 10 * (Math.pow(Math.cos(x * Math.PI / 180), 3));
        String bin = Integer.toBinaryString((int) y);
        int res = 0;
        int counter1 = 0;
        int counter2 = 0;
        for (int i = 0; i < bin.length(); i++) {
            if (bin.charAt(i) == '1') {
                counter1++;
            }

            if (bin.charAt(i) == '0') {

                if (counter1 > counter2) {
                    counter2 = counter1;

                }
                counter1 = 0;
            }

        }

        if (counter1 > counter2) {

            res = counter1;
        }

        else {

            res = counter2;
        }

        return res;
    }

    /**
     * Визначає значення функціі збережує результат обчислення у контейнер
     * {@linkplain ViewResult#container}
     *
     * @param x - аргумент обчисляємой функціі
     */
    public void Init(int x) {
        Item CalculatedObject = new Item();
        CalculatedObject.setX(x);
        CalculatedObject.setY(calculation(x));
        container.add(CalculatedObject);
    }


    /** Реалізація методу {@linkplain View # viewInit()} <br>
     * {@InheritDoc}
     * @param x - аргумент обчисляємой функціі
     */
    @Override
    public void viewInit(int x) {

        Init(x);
    }

    /** Реалізація методу {@linkplain View # viewSave()} <br>
     * {@InheritDoc}
     */
    @Override
    public void viewSave() throws IOException {
        ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
        os.writeObject(container);
        os.flush();
        os.close();
    }

    /** Реалізація методу {@linkplain View # viewRestore()} <br>
     * {@InheritDoc}
     */
    @Override
    public void viewRestore() throws FileNotFoundException, IOException, ClassNotFoundException {
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
        container = (ArrayList<Item>) is.readObject();
        is.close();

    }

    /** Реалізація методу {@linkplain View # viewHeader()} <br>
     * {@InheritDoc}
     */
    @Override
    public void viewHeader() {
        System.out.println("Result:");

    }

    /** Реалізація методу {@linkplain View # viewBody()} <br>
     * {@InheritDoc}
     */
    @Override
    public void viewBody() {
        for (Item s : container) {
            System.out.println(s.getY());
        }

    }

    /** Реалізація методу {@linkplain View # viewFooter()} <br>
     * {@InheritDoc}
     */
    @Override
    public void viewFooter() {
        System.out.println("End");

    }
    /** Реалізація методу {@linkplain View # viewShow()} <br>
     * {@InheritDoc}
     */
    @Override
    public void viewShow() {
        viewHeader();
        viewBody();
        viewFooter();

    }

}

