package ua.khpi.boychenko.lab15;

import java.util.HashSet;
import java.util.Set;

/**
 * Визначає засоби взаємодії спостерігачів і спостережуваних; шаблон Observer
 * 
 * @author Бойченко Олександра
 * @version 1.0
 * @see Observer
 */
public abstract class Observable {
	/**
	* Безліч спостерігачів; шаблон Observer
	*
	* @See Observer
	*/
	private Set<Observer> observers = new HashSet<Observer>();

	/**
	* Додає спостерігача; шаблон Observer
	*
	* @Param observer
	* Об'єкт-спостерігач
	*/
	public void addObserver(Observer observer) {
		observers.add(observer);
	}

	/**
	* Видаляє спостерігача; шаблон Observer
	*
	* @Param observer
	* Об'єкт-спостерігач
	*/
	public void delObserver(Observer observer) {
		observers.remove(observer);
	}

	/**
	* Попереджає спостерігачів про подію; шаблон Observer
	*
	* @Param event
	* Інформація про подію
	*/
	public void call(Object event) {
		for (Observer observer : observers) {
			observer.handleEvent(this, event);
		}
	}
}
