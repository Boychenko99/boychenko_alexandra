package ua.khpi.boychenko.lab15;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Анотація часу виконання для призначення методам спостерігача конкретних подій
 * 
 * @Author Бойченко Олександра
 * @See AnnotatedObserver  
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Event {
	String value();
}
