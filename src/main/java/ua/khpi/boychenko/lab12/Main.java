package ua.khpi.boychenko.lab12;

import java.util.Scanner;

import ua.khpi.boychenko.lab11.View;

/**
 * Обчислення і видображення результатів. Містить реалізацію статичного методу
 * main().
 * 
 * @author Бойченко Олександра
 * @version 3.0
 * @see Main#main
 */
public class Main extends ua.khpi.boychenko.lab11.Main {

	public Main(View view) {
		super(view);
	}

	/**
	 * Виконується при запуску програми. 
	 * Викликає метод
	 * 
	 * @param args - параметри запуску програми.
	 */
	public static void main(String[] args) {
		
		Main control = new Main((View) new ViewableTable().getView());
		control.menu();
	}

}

