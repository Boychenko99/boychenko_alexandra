package ua.khpi.boychenko.lab12;



import ua.khpi.boychenko.lab11.View;
import ua.khpi.boychenko.lab11.ViewableResult;

/** ConcreteCreator (Шаблон проектування Factory Method) <br>
 * Оголошує метод, "Фабрика", який створює об'єкти
 *
 * @Author Бойченко Олександра
 * @Version 1.0
 * @See ViewableResult
 * @See ViewableTable # getView ()
 */
public class ViewableTable extends ViewableResult {

	/** Створює відображаємий об'єкт {@linkplain ViewTable} */
	@Override
	public View getView() {
		return new ViewTable();
	}
}
