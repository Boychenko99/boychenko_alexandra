package ua.khpi.boychenko.lab10;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Містить реалізацію методів для розрахунку та відображення результатів
 * збереження і відновлення даних.
 *
 * @author Бойченко Александра
 * @version 1.0
 */
public class Calc {
    /** Ім'я файлу, яке використовується при сериалізации. */
    private static final String FNAME = "Item.bin";
    /** Зберігає результат обчислення. Об'єкт класу {@linkplain Item} */
    private Item result;

    /** Ініціалізує {@linkplain Calc#result} */
    public Calc() {
        result = new Item();
    }

    /**
     * Отримання значення {@linkplain Calc # result}
     *
     * @return поточне значення посилання на об'єкт {@linkplain Item}
     */
    public Item getResult() {
        return result;
    }

    /**
     * Встановка значення {@linkplain Calc # result} Результат
     *
     * @param -
     *            нове значення посилання на об'єкт {@linkplain Item}
     */
    public void setResult(Item res) {
        this.result = res;
    }

    /**
     * Обчислення наибольшої длини послідовності 1 в двоїчному представлення
     * цілісної суми квадрата і куби 10 * cos (α).
     *
     * @param x - аргумент обчисляємой функціі
     * @return res - результат обчислення
     */
    private int calculation(int x) {
        double y = 0;
        y = 10 * Math.pow(Math.cos(x * Math.PI / 180), 2) + 10 * (Math.pow(Math.cos(x * Math.PI / 180), 3));
        String bin = Integer.toBinaryString((int) y);
        int res = 0;
        int counter1 = 0;
        int counter2 = 0;
        for (int i = 0; i < bin.length(); i++) {
            if (bin.charAt(i) == '1') {
                counter1++;
            }

            if (bin.charAt(i) == '0') {

                if (counter1 > counter2) {
                    counter2 = counter1;

                }
                counter1 = 0;
            }

        }

        if (counter1 > counter2) {

            res = counter1;
        }

        else {

            res = counter2;
        }
        System.out.println(bin);
        return res;
    }

    /**
     * Визначає значення функціі збережує результат обчислення у об'єкт
     * {@linkplain Calc#result}
     *
     * @param x - аргумент обчисляємой функціі
     * @return результат обчислення
     */
    public int initialization(int x) {
        result.setX(x);
        result.setY(calculation(x));
        return result.getY();
    }

    /** Виводить результат обчислення. */
    public void show() {
        System.out.println(result.getY());
    }

    /**
     * Збережує {@linkplain Calc#result} у файлі {@linkplain Calc#FNAME}
     *
     * @throws IOException
     */
    public void save() throws IOException {
        ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
        os.writeObject(result);
        os.flush();
        os.close();
    }

    /**
     * Встановлює {@linkplain Calc#result} з файлу {@linkplain Calc#FNAME}
     *
     * @throws Exception
     */
    public void restore() throws Exception {
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
        result = (Item) is.readObject();
        is.close();
    }
}

