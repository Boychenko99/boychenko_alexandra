package ua.khpi.boychenko.lab16;


import ua.khpi.boychenko.lab11.View;

/**
* Обчислення и відображенням результату. Містіть реалізацію статичного методу
* main().
*
* @Author Бойченко Олександра
* @Version 7.0
* @See Main # main
*/
public class Main extends ua.khpi.boychenko.lab11.Main {
	public Main(View view) {
		super(view);
		
	}

	/**
	 * Виконується при запуску програми. 
	 *
	 * 
	 * @param args - параметри запуску програми.
	 */
	public static void main(String[] args) {
		Main control = new Main(new ViewableWindow().getView());
		control.menu();
		System.exit(0);
	}
}
