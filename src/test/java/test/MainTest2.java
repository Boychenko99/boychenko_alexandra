import org.junit.Test;
import junit.framework.Assert;
import ua.khpi.boychenko.lab11.Item;
import ua.khpi.boychenko.lab11.ViewResult;

import static org.junit.Assert.*;

import java.io.IOException;

/**
 * Виконує тестування розроблених класів.
 * 
 * @author Бойченко Олександра
 * @version 2.0
 */
public class MainTest2 {
	/** Перевірка основної функціональності класу {@linkplain ViewResult} */
	@Test
	public void testCalc() {
		ViewResult view = new ViewResult();
		Item testObject=new Item();
	
		int correctResult = 2;
		view.Init(5);
		testObject=view.getItems().get(0);
		assertEquals(correctResult, testObject.getY());
	
		correctResult = 1;
		view.Init(149);
		testObject=view.getItems().get(1);
		assertEquals(correctResult, testObject.getY());
	
	}

	/** Перевірка серілізації. Корректності відновленних даних. */
	@Test
	public void testRestore() {
		ViewResult view1 = new ViewResult();
		ViewResult view2 = new ViewResult();
		int correctResult1 = 2;
		int correctResult2 = 1;
		view1.Init(5);
		view1.Init(149);
		try {
			view1.viewSave();
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
		
		try {
			view2.viewRestore();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		assertEquals(view1.getItems().size(), view2.getItems().size());

	}
}
