import static org.junit.Assert.*;
import ua.khpi.boychenko.lab13.*;
import ua.khpi.boychenko.lab11.*;

import org.junit.Test;

/**
 * Тестування класів FindConsoleCommand та DeleteItemConsoleCommand
 * 
 * @Author Бойченко Олександра
 * @Version 4.0
 * @See ChangeItemCommand
 */
public class CommandTest {

	/** Перевірка метода {@linkplain FindConsoleCommand} */
	@Test
	public void findElementCommand() {
		View view = new ViewResult();

		Item correctItem = new Item();
		Item testItem = new Item();
		correctItem.setX(10);
		correctItem.setY(2);
		view.viewInit(5);
		view.viewInit(8);
		view.viewInit(9);
		view.viewInit(2);
		view.viewInit(10);
		FindConsoleCommand cmd = new FindConsoleCommand(view);
		testItem = cmd.search(4);

		assertEquals((int) correctItem.getX(), (int) testItem.getX());
		assertEquals(correctItem.getY(), testItem.getY());
	}

	/** Перевірка метода {@linkplain DeleteItemConsoleCommand#execute()} */
	@Test
	public void testDeleteCommand() {
		View view = new ViewResult();

		int sizeBeforeDelete = 0;
		int sizeAfterDelete = 0;
		view.viewInit(5);
		view.viewInit(8);
		view.viewInit(9);
		view.viewInit(2);
		view.viewInit(10);
		DeleteItemConsoleCommand cmd = new DeleteItemConsoleCommand(view);
		sizeBeforeDelete = ((ViewResult) view).getItems().size();
		cmd.execute();
		sizeAfterDelete = ((ViewResult) view).getItems().size();

		assertEquals(sizeBeforeDelete, sizeAfterDelete + 1);

	}

}
