import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import junit.framework.Assert;
import ua.khpi.boychenko.lab11.Item;
import ua.khpi.boychenko.lab11.ViewResult;
import ua.khpi.boychenko.lab12.ViewTable;

/**
 * Виконує тестування розроблених класів.
 * 
 * @author Бойченко Олександра
 * @version 3.0
 */
public class MainTest3 {
	/** Перевірка основної функціональності класу {@linkplain ViewResult} */
	@Test
	public void testCalc() {
		ViewTable tbl = new ViewTable(10);
		 assertEquals(10, tbl.getWidth());
		ViewTable view = new ViewTable();
		Item testObject=new Item();
	
		int correctResult = 1;
		view.Init(149, 20);
		testObject=view.getItems().get(0);
		view.viewShow();
		assertEquals(correctResult, testObject.getY());
	
		correctResult = 2;
		view.Init(5);
		testObject=view.getItems().get(1);
		assertEquals(correctResult, testObject.getY());
	

	}

	/** Перевірка серілізації. Корректності відновленних даних. */
	@Test
	public void testRestore() {
		ViewTable view1 = new ViewTable();
		ViewTable view2 = new ViewTable();
		int correctResult1 = 2;
		int correctResult2 = 1;

		view1.Init(5);
		view1.Init(149);
		try {
			view1.viewSave();
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
		
		try {
			view2.viewRestore();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		assertEquals(view1.getItems().size(), view2.getItems().size());

	}
}
